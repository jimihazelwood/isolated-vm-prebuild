const execSync = require('child_process').execSync;
const path = require('path');
const fs = require('fs');
const os = require('os');
const mkdirp = require('mkdirp');

const buildPathSource = path.join(process.cwd(), 'node_modules', 'isolated-vm', 'build', 'Release', 'isolated_vm.node');
const buildPathTarget = path.join(process.cwd(), 'build', os.arch(), os.platform());
mkdirp.sync(buildPathTarget);
fs.copyFileSync(buildPathSource, path.join(buildPathTarget, 'isolated_vm.node'));
