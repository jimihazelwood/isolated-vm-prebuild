const os = require('os');
const fs = require('fs');
const path = require('path');

if (process.versions.node.indexOf('12.') !== 0) {
  throw new Error('The sandbox requires node 12 to run');
}

const prebuildDirectory = path.join(__dirname, 'build', os.arch(), os.platform());
if (!fs.existsSync(prebuildDirectory)) {
  throw new Error('The sandbox is not supported on this platform');
}

module.exports = require(path.join(prebuildDirectory, 'isolated_vm.node')).ivm;
